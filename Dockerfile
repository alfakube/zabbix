FROM alpine:latest
LABEL maintaner="Roman Stolyarchuk <stolyarchuk@live.ru>"

RUN apk add --update \
    tzdata lighttpd php7-common php7-iconv php7-json php7-gd php7-bcmath php7-xmlreader php7-mbstring \
    php7-curl php7-xml php7-pgsql php7-imap php7-cgi fcgi php7-pdo php7-pdo_pgsql php7-sockets \
    php7-soap php7-xmlrpc php7-posix php7-mcrypt php7-gettext php7-ldap php7-ctype php7-dom \
    bash zabbix zabbix-pgsql zabbix-webif zabbix-agent supervisor syslog-ng curl && \
    cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo Europe/Moscow > /etc/TZ && \
    rm -rf /var/www/localhost/htdocs && \
    ln -sf /usr/share/webapps/zabbix /var/www/localhost && \
    ln -sf /usr/bin/php-cgi7 /usr/bin/php-cgi && \
    chmod u+s /usr/sbin/fping && \
    chown -R lighttpd /usr/share/webapps/zabbix/conf && \
    mkdir -p /run/lighttpd && chown lighttpd /run/lighttpd && \
    apk del tzdata && rm -rf /var/cache/apk/*

COPY etc/ /etc/

CMD ["/usr/bin/supervisord","-n"]
